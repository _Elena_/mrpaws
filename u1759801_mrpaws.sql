-- MySQL dump 10.13  Distrib 5.7.27-30, for Linux (x86_64)
--
-- Host: localhost    Database: u1759801_mrpaws
-- ------------------------------------------------------
-- Server version	5.7.27-30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*!50717 SELECT COUNT(*) INTO @rocksdb_has_p_s_session_variables FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'performance_schema' AND TABLE_NAME = 'session_variables' */;
/*!50717 SET @rocksdb_get_is_supported = IF (@rocksdb_has_p_s_session_variables, 'SELECT COUNT(*) INTO @rocksdb_is_supported FROM performance_schema.session_variables WHERE VARIABLE_NAME=\'rocksdb_bulk_load\'', 'SELECT 0') */;
/*!50717 PREPARE s FROM @rocksdb_get_is_supported */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;
/*!50717 SET @rocksdb_enable_bulk_load = IF (@rocksdb_is_supported, 'SET SESSION rocksdb_bulk_load = 1', 'SET @rocksdb_dummy_bulk_load = 0') */;
/*!50717 PREPARE s FROM @rocksdb_enable_bulk_load */;
/*!50717 EXECUTE s */;
/*!50717 DEALLOCATE PREPARE s */;

--
-- Table structure for table `breed`
--

DROP TABLE IF EXISTS `breed`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `breed` (
  `breed_id` int(3) NOT NULL AUTO_INCREMENT,
  `breed_name` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_shortname` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_group` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_category` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_origin` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_history` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_image` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`breed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `breed`
--

LOCK TABLES `breed` WRITE;
/*!40000 ALTER TABLE `breed` DISABLE KEYS */;
INSERT INTO `breed` VALUES (1,'Мемная','МЕМ','Полудлинношёрстная','Дополнительная','Япония','История здесь','breed_1.jpg'),(2,'Серьёзная','СРЗ','Короткошёрстная','Иная','Россия','История тут','breed_2.jpg'),(3,'Базированная','БАЗ','Длинношёрстная','Дополнительная','Великобритания','История там','breed_3.jpg'),(4,'Кринжовая','КРЖ','Полудлинношёрстная','Основная','Индия','Истории нет хех','breed_4.jpg');
/*!40000 ALTER TABLE `breed` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cats`
--

DROP TABLE IF EXISTS `cats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cats` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `breed_id` int(3) NOT NULL,
  `users_id` int(3) NOT NULL,
  `sex` char(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `birth` int(2) NOT NULL,
  `picture` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `users_id` (`users_id`),
  KEY `breed_id` (`breed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cats`
--

LOCK TABLES `cats` WRITE;
/*!40000 ALTER TABLE `cats` DISABLE KEYS */;
INSERT INTO `cats` VALUES (1,'Тигр',1,2,'мужской','коричневый',5,'image_3.jpg'),(2,'Барсик',2,10,'мужской','сиамский',3,'image_6.jpg'),(3,'Шпунтик',3,5,'мужской','трехцветный',2,'image_1.jpg'),(4,'Снежок',4,9,'женский','коричневый',5,'image_2.jpg'),(5,'Леопольд',1,7,'мужской','сиамский',1,'image_5.jpg'),(6,'Барсик',3,2,'женский','черный',5,'image_5.jpg'),(7,'Матроскин',2,8,'мужской','сиамский',4,'image_3.jpg'),(8,'Карамелька',1,8,'женский','полосатый',8,'image_4.jpg'),(9,'Тигр',3,2,'мужской','голубоглазый',5,'image_5.jpg'),(10,'Кнопка',1,4,'женский','голубоглазый',5,'image_1.jpg'),(11,'Пират',2,8,'мужской','голубоглазый',6,'image_1.jpg'),(12,'Хвостик',4,9,'мужской','серый',1,'image_5.jpg'),(14,'Киса',2,6,'женский','голубоглазый',4,'image_6.jpg'),(15,'Бублик',1,9,'женский','коричневый',3,'image_2.jpg'),(16,'Карамелька',1,6,'мужской','черный',8,'image_6.jpg'),(17,'The КОТ',3,3,'женский','казахский',3,'44171c1cea9e6bcd18f1546a7069e654.png'),(18,'Васька',3,5,'женский','белый',6,'image_4.jpg'),(19,'Пушинка',4,6,'мужской','черный',4,'image_6.jpg'),(20,'Серый',3,2,'мужской','белый',6,'image_2.jpg'),(21,'Бублик',3,7,'женский','рыжий',7,'image_5.jpg'),(22,'Пушок',1,8,'мужской','серый',8,'image_1.jpg'),(23,'Бублик',1,4,'женский','голубоглазый',6,'image_2.jpg'),(24,'Снежок',3,1,'женский','сиамский',5,'image_4.jpg'),(25,'Кнопа',2,4,'мужской','полосатый',6,'image_4.jpg'),(26,'Барбос',4,10,'мужской','коричневый',7,'image_2.jpg'),(27,'Мурка',1,10,'женский','трехцветный',5,'image_5.jpg'),(28,'Мурка',4,9,'мужской','коричневый',5,'image_4.jpg'),(29,'Киса',4,6,'мужской','серый',6,'image_4.jpg'),(30,'Барбос',4,2,'мужской','черный',6,'image_4.jpg'),(31,'Кузя',4,2,'мужской','трехцветный',4,'image_1.jpg'),(32,'Барсик',1,8,'женский','коричневый',3,'image_3.jpg'),(33,'Серый',1,2,'женский','полосатый',2,'image_3.jpg'),(34,'Кнопа',4,9,'мужской','рыжий',2,'image_3.jpg'),(35,'Матроскин',1,7,'женский','черный',1,'image_3.jpg'),(36,'Yesh',3,3,'мужской','Есть',5,'7a92702e43f5ce4865f3eb5fabeeb4af.jpg'),(37,'Д̵͓̦̃Р̷̦̓͗У̶̥̐͘Г̸̼̞̄',1,3,'мужской','???',0,'58fd252fd5eff4843192.webp');
/*!40000 ALTER TABLE `cats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6),
  `picture` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
INSERT INTO `news` VALUES (1,'Новость 1','<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolor mollitia maiores rem culpa accusamus alias cum, voluptate voluptates enim laboriosam sit, atque, itaque explicabo obcaecati est adipisci quibusdam voluptas fuga!</p>\r\n<p>Ab illum officia quae deserunt molestias repudiandae quas aliquid nihil ut fugit facere placeat veritatis ipsum rerum eum itaque nemo, veniam ratione similique saepe praesentium dignissimos animi a laborum. Quia!</p>','2023-03-29 07:08:40.202077','art_1.webp'),(2,'Новость 2','<p>Perspiciatis similique dignissimos id adipisci enim illum quasi eum quia perferendis est odio praesentium veniam eligendi dolores, sint commodi ipsa eveniet iusto ipsum ex voluptatem? Velit sint ex aliquam eum.</p>\r\n<p>Enim facere eveniet quia tempore illo. Voluptate, natus magni, voluptatibus officia sed officiis alias minima tenetur possimus perferendis dolorum dolorem a veritatis molestias dolore omnis obcaecati illum, neque placeat nam.</p>','2023-03-29 07:08:33.035763','art_2.jpg'),(3,'Новость 3','<p>Illum illo, totam corrupti deleniti voluptatem tempore doloremque assumenda obcaecati sequi nesciunt commodi dolore, numquam repellat nobis laborum amet. Omnis libero reiciendis rem vitae similique debitis inventore at dolorem eaque!</p>\r\n<p>Voluptatem odit, omnis perferendis est aspernatur inventore cum eos odio laborum voluptatum dolorum adipisci saepe itaque modi qui corporis et iste consectetur quis excepturi. Soluta ea provident maiores assumenda magnam?</p>','2023-03-29 07:08:19.498486','art_3.webp'),(4,'Новость 4','<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. A excepturi placeat soluta ab non explicabo dolorum, sapiente cum voluptas. Quasi, excepturi illum laboriosam deleniti eius beatae eligendi? Quod, neque distinctio.</p>\r\n      <p>Voluptas impedit harum, dolores perferendis expedita praesentium? Iste at magnam corrupti laudantium, corporis, explicabo dolores quos fuga tempore debitis aliquam! Dolorum saepe quae corrupti voluptatibus mollitia blanditiis voluptates voluptate necessitatibus!</p>\r\n      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam illo suscipit fugiat voluptatum autem nostrum distinctio minus. Dolore ab eos nihil facere alias sit quaerat neque veniam, aut recusandae modi.</p>','2023-03-29 07:06:39.000000','art_4.jpg');
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT 'Не указано',
  `user_email` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_pass` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_phone` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT 'Не указан',
  `user_country` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_city` varchar(24) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_info` text COLLATE utf8mb4_unicode_ci,
  `user_image` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'default_user.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'admin','admin@mail.ru','pass','+79171463590','Россия','Омск','Администратор сего чуда.','default_user.jpg'),(2,'Оксана','oksana@mail.ru','1234','Не указан','Россия','Омск','Пользователь этого сайта, текста про которого нет.','default_user.jpg'),(3,'Just Quin','quin@mail.ru','pass','3423424','Империя Казахстан','Астана','Some info','8608a8efa99da5181994.jpg'),(4,'Оскар','oscar@mail.ru','userpass','+79171019977','Россия','Омск','Информация о пользователе здесь','default_user.jpg'),(5,'Елена','elena@mail.ru','2023','+79171028866','Россия','Москва','Информация о пользователе здесь','default_user.jpg'),(6,'Анастасия','anastasia@mail.ru','password','+79171342573','Россия','Омск','Информация о пользователе здесь','default_user.jpg'),(7,'НеИмя','anon@mail.ru','12345678','+79176426373','Россия','Омск','Информация о пользователе здесь','default_user.jpg'),(8,'Даниил','dan@mail.ru','87654321','+79179477492','Россия','Тольятти','Информация о пользователе здесь','default_user.jpg'),(9,'Гена','gena@mail.ru','11111111','+79176439352','Россия','Екатеринбург','Информация о пользователе здесь','default_user.jpg'),(10,'Кристина','kris@mail.ru','memes','+79177831124','Россия','Омск','Информация о пользователе здесь','default_user.jpg'),(13,'123','this@mail.com','123','',NULL,NULL,NULL,'default_user.jpg');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!50112 SET @disable_bulk_load = IF (@is_rocksdb_supported, 'SET SESSION rocksdb_bulk_load = @old_rocksdb_bulk_load', 'SET @dummy_rocksdb_bulk_load = 0') */;
/*!50112 PREPARE s FROM @disable_bulk_load */;
/*!50112 EXECUTE s */;
/*!50112 DEALLOCATE PREPARE s */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-01  7:05:54
