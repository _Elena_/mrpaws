<?php
require 'header.php';
$query = "";
if(!empty($_COOKIE['user'])){
    $loggedUser = $_COOKIE['user'];
    $thisQuery = "SELECT * FROM users WHERE user_email LIKE '$loggedUser';";
    $query = $thisQuery;
    $userId = null;
    $result = mysqli_query($connection, $query);
    $u_name = '';
    $u_country = '';
    $u_city = '';
    $u_info = '';
    $u_email = '';
    $u_pass = '';
    $u_image = '';
    while($userRow = mysqli_fetch_assoc($result)){
        $userId = $userRow['id'];
        $u_name = $userRow['user_name'];
        $u_country = $userRow['user_country'];
        $u_city = $userRow['user_city'];
        $u_info = $userRow['user_info'];
        $u_phone = $userRow['user_phone'];
        $u_image = $userRow['user_image'];
        $u_email = $userRow['user_email'];
        $u_pass = $userRow['user_pass'];
    }
?>
<main class="main">
<section class="profile container">
    <div class="profile-inner">
      <div class="profile__info">
        <div class="profile__info-part">
          <div class="profile__info-img"><img src="assets/img/users/<?php echo $u_image; ?>" alt="#"></div>
        </div>
        <div class="profile__info-part">
          <div class="profile__info-name"> <span><?php echo $u_name; ?></span>
          <?php
          if( !empty($loggedUser) && $u_email == $loggedUser ){
          ?>
          <a href="profile.php" style="font-size:14px;color:#4b847d;">Вернуться в профиль</a></div>
          <?php
          }
          ?>
          <div class="profile__info-location">
            <svg width="21" height="32" viewBox="0 0 21 32" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.3201 31.2C8.93008 29 7.54008 26.77 6.16008 24.55C4.0903 21.4382 2.34136 18.1246 0.940083 14.66C-0.102955 12.2383 -0.262226 9.52716 0.490083 6.99999C1.26691 4.91175 2.67339 3.11648 4.51502 1.86245C6.35666 0.608418 8.54244 -0.042402 10.7701 -5.87758e-06C12.9513 0.0881099 15.0537 0.840065 16.796 2.15531C18.5384 3.47055 19.8376 5.28638 20.5201 7.35999C21.1365 9.76252 20.954 12.3004 20.0001 14.59C18.7566 17.7443 17.1882 20.7605 15.3201 23.59C13.8701 25.94 12.3801 28.27 10.9101 30.59C10.8001 30.77 10.7001 30.97 10.6001 31.16L10.3201 31.2ZM14.3201 10.45C14.3025 9.44394 13.8977 8.4834 13.1899 7.76819C12.4821 7.05298 11.5259 6.63815 10.5201 6.60999C9.50374 6.62018 8.5313 7.02573 7.80888 7.74069C7.08646 8.45564 6.67083 9.42381 6.65008 10.44C6.6677 11.446 7.0725 12.4066 7.78026 13.1218C8.48802 13.837 9.44427 14.2518 10.4501 14.28C11.4672 14.2723 12.4411 13.8676 13.1641 13.1521C13.887 12.4366 14.3019 11.467 14.3201 10.45Z" fill="#DAC2A8"/>
            </svg>
            <?php 
            if(!empty($u_city) && $u_country){ 
            ?>
              <span><?php echo $u_city; ?>, <?php echo $u_country; ?></span>
            <?php
            } else if(!empty($u_city) && empty($u_country)){
            ?>
              <span><?php echo $u_city; ?></span>
            <?php 
            } else if(empty($u_city) && !empty($u_country)){
            ?>
              <span><?php echo $u_country; ?></span>
            <?php 
            } else{
            ?>
              <span>Не указано</span>
            <?php 
            }
            ?>
          </div>
        <div class="profile__info-about"><span>О пользователе</span>
          <p><?php echo $u_info; ?></p>
        </div>
        <div class="profile__info-contacts"><span>Контакты:</span>
          <p></p>
          <p></p>
        </div>
      </div>
    </div>
  </section>
    <style>
        .update-button{
            display: block;
            margin: 24px auto 0;
            font-size: 22px;
            background-color: #996494;
            border: none;
            border-radius: 0;
            color: white;
            padding: 19px 24px;
        }
        a.update-button{
            width: fit-content;
        }
    </style>
    <section class="user container" id="updateuser">
        <h2>Изменить основную информацию</h2>
        <?php
            if(isset($_GET['updid']) && $_GET['updid'] == '1'){
                if($_GET['status']=="green"){
                    echo '<p class="sysmessage" style="background-color: green;color:white;">Профиль успешно обновлён.</p>';
                } else if($_GET['status']=="yellow"){
                    // echo '<p class="sysmessage" style="background-color: yellow;">Вы указали неверный пароль. Попробуйте ещё раз.</p>';
                } else if($_GET['status']=="red"){
                    echo '<p class="sysmessage" style="background-color: red;color:white;">Что-то пошло не так.</p>';
                }
            }
        ?>
        <form action="crud.php" method="POST" enctype=multipart/form-data>
            <div class="update-input">
                <p><storng>Имя:</storng> <?php echo $u_name; ?></p>
                <div>
                    <label for="new_u_name">Сменить на: </label>
                    <input type="text" id="new_u_name" name="user_name">
                </div>
            </div>
            <div class="update-input">
                <p><storng>Страна:</storng> <?php if(!empty($u_country)): echo $u_country; else: echo "Не указано"; endif; ?></p>
                <div>
                    <label for="new_u_country">Сменить на: </label>
                    <input type="text" id="new_u_country" name="user_country">
                </div>
            </div>
            <div class="update-input">
                <p><storng>Город:</storng> <?php if(!empty($u_city)): echo $u_city; else: echo "Не указано"; endif; ?></p>
                <div>
                    <label for="new_u_city">Сменить на: </label>
                    <input type="text" id="new_u_city" name="user_city">
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_u_info">Информация о пользователе: </label>
                    <textarea type="text" id="new_u_info" name="user_info"><?php echo $u_info; ?></textarea>
                </div>
            </div>
            <div class="update-input">
                <p><storng>Телефон:</storng> <?php if(!empty($u_phone)): echo $u_phone; else: echo "Не указано"; endif; ?></p>
                <div>
                    <label for="new_u_phone">Изменить номер: </label>
                    <input type="tel" id="new_u_phone" name="user_phone">
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_u_image">Загрузить фотографию: </label>
                    <input id="new_u_image" name="user_image" type="file" accept="image/*">
                </div>
            </div>
            <div>
                <input type="text" hidden name="user_id" value="<?php echo $userId; ?>">
                <button class="update-button" id="update_user_submit" type="submit" value="update_user" name="update_user">Добавить</button>
            </div>
        </form>
        <hr>
        <h2>Сменить почту</h2>
        <form action="crud.php" method="POST" id="updateemail">
            <?php
                if(isset($_GET['updid']) && $_GET['updid'] == '2'){
                    if($_GET['status']=="green"){
                        echo '<p class="sysmessage" style="background-color: green;color:white;">Почта успешно обновлена.</p>';
                    } else if($_GET['status']=="yellow"){
                        echo '<p class="sysmessage" style="background-color: yellow;">Вы указали неверный пароль. Попробуйте ещё раз.</p>';
                    } else if($_GET['status']=="red"){
                        echo '<p class="sysmessage" style="background-color: red;color:white;">Что-то пошло не так.</p>';
                    }
                }
            ?>
            <div class="update-input">
                <p><storng>Email:</storng> <?php echo $u_email; ?></p>
                <div>
                    <label for="new_u_email">Новая почта: </label>
                    <input id="new_u_email" type="email" name="user_email" required>
                </div>
                <div>
                    <label for="old_u_pass">Текущий пароль: </label>
                    <input type="password" id="old_u_pass" name="user_pass_old" required>
                </div>
            </div>
            <div>
                <input type="text" hidden name="user_id" value="<?php echo $userId; ?>">
                <button class="update-button" id="update_email" type="submit" value="update_email" name="update_email">Добавить</button>
            </div>
        </form>
        <form action="crud.php" method="POST" id="updatepass">
            <div class="update-input">
                <?php
                if(isset($_GET['updid']) && $_GET['updid'] == '3'){
                    if($_GET['status']=="green"){
                        echo '<p class="sysmessage" style="background-color: green;color:white;">Пароль успешно обновлён</p>';
                    } else if($_GET['status']=="yellow"){
                        echo '<p class="sysmessage" style="background-color: yellow;">Вы указали неверный текущий пароль или попытались сменить пароль на тот же. Попробуйте ещё раз.</p>';
                    } else if($_GET['status']=="red"){
                        echo '<p class="sysmessage" style="background-color: red;color:white;">Что-то пошло не так</p>';
                    }
                }
                ?>
                <p>Сменить пароль: </p>
                <div>
                    <label for="old_u_pass">Текущий пароль: </label>
                    <input type="password" id="old_u_pass" name="user_pass_old" required>
                </div>
                <div>
                    <label for="new_u_pass">Новый пароль: </label>
                    <input type="password" id="new_u_pass" name="user_pass" required>
                </div>
            </div>
                <div>
                <input hidden type="text" name="user_id" value="<?php echo $userId; ?>" required>
                <button class="update-button" id="update_pass" type="submit" value="update_pass" name="update_pass">Добавить</button>
            </div>
        </form>
    </section>
    <section class="cats">
        <h2 class="section-title">Ваши коты и кошки:</h2>
        <div class="cats-inner container">
            <ul class="cats__list" id="paginated-list" style="grid-template-rows: auto;" data-current-page="1">
                <?php
                $query = "SELECT * FROM cats WHERE users_id LIKE '$userId';";
                $result = mysqli_query($connection, $query);
                    while($catsRow = mysqli_fetch_assoc($result)){
                ?>
                <li class="cats__list-item"> 
                    <div class="cats-img" style="overflow:hidden;margin:0 auto;">
                        <img style="width:100%;height:100%;object-fit:cover;" src="assets/img/cats/<?php echo $catsRow['picture']; ?>" alt="#">
                    </div>
                    <a href="cat.php?cat=<?php echo $catsRow['id']; ?>" class="cats__info">
                        <h3 class="cats__info-title"><?php echo $catsRow['name']; ?></h3>
                    </a>
                    <a class="update-button" href="editcat.php?cat=<?php echo $catsRow['id']; ?>">Изменить запись</a>
                    <form action="crud.php" method="POST">
                        <input type="text" hidden name="cat_id_to_del" value="<?php echo $catsRow['id']; ?>">
                        <input type="text" hidden name="user_id_to_del" value="<?php echo $userId; ?>">
                        <button style="background-color: #ff3b3b;color:white;margin:18px auto 0;border:none;padding:4px 8px;display:block;" type="submit" name="delete_cat">Удалить запись</button>
                    </form>
                </li>
                <?
                }
                ?>
            </ul>
        </div>
        <div class="pagination-container">
            <button class="pagination-button" id="prev-button">&lt;</button>
            <div id="pagination-numbers"></div>
            <button class="pagination-button" id="next-button">&gt;</button>
        </div>
    </section>
<script src="assets/js/cats.js"></script>
    <style>
        .update-input{
            border-bottom: 1px solid black;
            padding-bottom: 16px;
            padding-top: 16px;
        }
    </style>
    <section class="cat container">
    <style>
        .update-input{
            border-bottom: 1px solid black;
            padding-bottom: 16px;
            font-size: 22px;
            /* color: white;
            background-color: #4b847d;
            padding: 10px 12px; */
            /* margin-bottom: 12px; */
        }
        .update-input input, .update-input select{
            border: none;
            padding: 8px 10px;
            font-size: 22px;
        }
        .update-input div{
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            align-items: center;
            color: white;
            background-color: #4b847d;
            padding: 10px 12px;
        }
        button[type="submit"]#create_submit{
            display: block;
            margin: 24px auto 0;
            font-size: 42px;
            background-color: #996494;
            border: none;
            border-radius: 0;
            color: white;
            padding: 19px 24px;
        }
    </style>
        <h2>Добавить кота</h2>
        <h3>Все поля обязательны для заполнения</h3>
        <form action="crud.php" method="POST" enctype=multipart/form-data>
            <div class="update-input">
                <div>
                    <label for="new_c_name">Имя: </label>
                    <input type="text" id="new_c_name" name="cat_name" required>
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_c_age">Возраст: </label>
                    <input type="text" id="new_c_age" name="cat_age" required>
                </div>
            </div>
            <div class="update-input">
                <div>
                <label for="new_c_breed">Порода: </label>
                    <select id="new_c_breed" name="cat_breed" required>
                        <?php
                        $breeds_query = "SELECT * FROM breed";
                        $breeds_result = mysqli_query($connection, $breeds_query);
                        while($breedsRow = mysqli_fetch_assoc($breeds_result)){
                            ?>
                            <option value="<?php echo $breedsRow['breed_id']; ?>"><?php echo $breedsRow['breed_name']; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_c_color">Окрас: </label>
                    <input type="text" id="new_c_color" name="cat_color" required>
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_c_sex">Пол: </label>
                    <select id="new_c_sex" name="cat_sex" required>
                        <option value="мужской">мужской</option>
                        <option value="женский">женский</option>
                    </select>
                </div>
            </div>
            <div class="update-input">
                <div>
                    <label for="new_c_image">Загрузить фотографию: </label>
                    <input id="new_c_image" name="cat_image" type="file" accept="image/*" required>
                </div>
            </div>
            <div style="display:none;">
                <input type="text" name="user_id" value="<?php echo $userId; ?>" required>
            </div>
            <div>
                <button id="create_submit" type="submit" value="create" name="create_cat">Добавить</button>
            </div>
        </form>
    </section>
<?php
    }
require 'footer.php';
?>