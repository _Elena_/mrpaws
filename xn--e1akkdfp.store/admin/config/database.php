<?php

class Database
{
    // укажите свои собственные учетные данные для базы данных
    private $host = "localhost";
    private $db_name = "u1759801_mrpaws";
    private $username = "u1759801_mrpaws";
    private $password = "lT3qP0bS6ixQ9lQ9";
    public $conn;

    // получение соединения с базой данных
    public function getConnection()
    {
        $this->conn = null;

        try {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name . ";charset=utf8mb4", $this->username, $this->password);
        } catch (PDOException $exception) {
            echo "Ошибка соединения: " . $exception->getMessage();
        }

        return $this->conn;
    }
}