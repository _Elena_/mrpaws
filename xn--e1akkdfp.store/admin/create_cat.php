<?php
// подключим файлы, необходимые для подключения к базе данных и файлы с объектами
include_once "config/database.php";
include_once "objects/cats.php";
include_once "objects/users.php";
include_once "objects/breeds.php";

// получаем соединение с базой данных
$database = new Database();
$db = $database->getConnection();

// создадим экземпляры классов Product и Category
$cat = new Cat($db);
$user = new User($db);
$breed = new Breed($db);

// установка заголовка страницы
$page_title = "Создание кота";

require_once "header.php";
?>

<div class="right-button-margin">
    <a href="index.php" class="btn btn-default pull-right">Просмотр всех котов</a>
</div>

<?php
// если форма была отправлена
if ($_POST) 
{
    // установим значения свойствам товара
    $cat->name = $_POST['name'];
    $cat->breed_id = $_POST['breed'];
    $cat->users_id = $_POST['user'];
    $cat->color = $_POST['color'];
    $cat->birth = $_POST['birth'];
    $cat->sex = $_POST['sex'];

    // создание товара
    if ($cat->create()) {
        echo '<div class="alert alert-success">Товар был успешно создан.</div>';
    }

    // если не удается создать товар, сообщим об этом пользователю
    else {
        echo '<div class="alert alert-danger">Невозможно создать товар.</div>';
    }
}
?>

<!-- HTML-формы для создания товара -->
<form action="<?= htmlspecialchars($_SERVER["PHP_SELF"]) ?>" method="post">
  
    <table class="table table-hover table-responsive table-bordered">
  
        <tr>
            <td>Имя</td>
            <td><input type="text" name="name" class="form-control" /></td>
        </tr>
  
        <tr>
            <td>Порода</td>
            <td>
            <?php
            // читаем породы базы данных
            $stmt = $breed->read();
            
            // помещаем их в выпадающий список
            echo "<select class='form-control' name='breed'>";
            echo "<option>Выбрать породу...</option>";
            
            while ($breeds = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($breeds);
                echo "<option value='{$breed_id}'>{$breed_name}</option>";
            }
            echo "</select>";
            ?></td>
        </tr>
     <tr>
            <td>Владелец</td>
            <td>
            <?php
            // читаем породы базы данных
            $stmt = $user->read();
            
            // помещаем их в выпадающий список
            echo "<select class='form-control' name='user'>";
            echo "<option>Выбрать владельца...</option>";
            
            while ($users = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($users);
                echo "<option value='{$id}'>{$user_name} ({$id})</option>";
            }
            
            echo "</select>";
            ?>
            </td>
        </tr>
        <tr>
            <td>Окрас</td>
            <td><input type="text" name="color" class="form-control"></input></td>
        </tr>
        <tr>
            <td>Возраст</td>
            <td><input type="text" name="birth" class="form-control"></input></td>
        </tr>
        <tr>
            <td>Пол</td>
            <td><input type="text" name="sex" class="form-control"></input></td>
        </tr>

        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Создать</button>
            </td>
        </tr>
  
    </table>
</form>
<?php // подвал
require_once "footer.php";
?>