<?php

// проверим, было ли получено значение в $_POST
if ($_POST) {

    // подключаем файлы для работы с базой данных и файлы с объектами
    include_once "config/database.php";
    include_once "objects/cats.php";

    // получаем соединение с базой данных
    $database = new Database();
    $db = $database->getConnection();

    // подготавливаем объект Product
    $cat = new Cat($db);

    // устанавливаем ID товара для удаления
    $cat->id = $_POST["delete_id"];

    // удаляем товар
    if ($cat->delete()) {
        echo "Кот был удалён";
        echo "<a style='display:block;' href='/admin'> Назад </a>";
    }

    // если невозможно удалить товар
    else {
        echo "Невозможно удалить Кота";
    }
}