<?php

// содержит переменные пагинации
include_once "config/core.php";

// файлы для работы с БД и файлы с объектами
include_once "config/database.php";
include_once "objects/cats.php";
include_once "objects/users.php";
include_once "objects/breeds.php";

// получение соединения с БД
$database = new Database();
$db = $database->getConnection();

$cat = new Cat($db);
$user = new User($db);
$breed = new Breed($db);

$page_title = "Список котов";

include_once "header.php";

// получение товаров
$stmt = $cat->readAll($from_record_num, $records_per_page);

// укажем страницу, на которой используется пагинация
$page_url = "index.php?";

// подсчёт общего количества строк (используется для разбивки на страницы)
$total_rows = $cat->countAll();

// контролирует, как будет отображаться список продуктов
include_once "read_template.php";

// содержит наш JavaScript и закрывающие теги html
include_once "footer.php";