<?php

class Breed
{
    // подключение к базе данных и имя таблицы
    private $conn;
    private $table_name = "breed";

    // свойства объекта
    public $id;
    public $breed_name;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // данный метод используется в раскрывающемся списке
    function read()
    {
        // запрос MySQL: выбираем столбцы в таблице «categories»
        $query = "SELECT
                    breed_id, breed_name, breed_shortname, breed_group, breed_category, breed_origin, breed_history
                FROM
                    " . $this->table_name . "
                ORDER BY
                    breed_name";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }
    // получение названия категории по её ID
    function readName()
    {
        // запрос MySQL
        $query = "SELECT breed_name FROM " . $this->table_name . " WHERE breed_id = ? limit 0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->breed_name = $row["breed_name"];
    }
    
}