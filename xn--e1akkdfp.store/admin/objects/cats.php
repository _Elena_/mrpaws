<?php

class Cat
{
    // подключение к базе данных и имя таблицы
    private $conn;
    private $table_name = "cats";

    // свойства объекта
    public $id;
    public $name;
    public $breed_id;
    public $users_id;
    public $sex;
    public $color;
    public $birth;
    public $picture;
    public function __construct($db)
    {
        $this->conn = $db;
    }

    // метод создания товара
    function create()
    {
        // запрос MySQL для вставки записей в таблицу БД «products»
        $query = "INSERT INTO 
                    " . $this->table_name . "
                SET
                id=:id, name=:name, breed_id=:breed_id, users_id=:users_id, sex=:sex, color=:color, birth=:birth, picture=:picture";

        $stmt = $this->conn->prepare($query);

        // опубликованные значения
        $this->id = htmlspecialchars(strip_tags($this->id));
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->breed_id = htmlspecialchars(strip_tags($this->breed_id));
        $this->users_id = htmlspecialchars(strip_tags($this->users_id));
        $this->sex = htmlspecialchars(strip_tags($this->sex));
        $this->color = htmlspecialchars(strip_tags($this->color));
        $this->birth = htmlspecialchars(strip_tags($this->birth));



        // привязываем значения
        $stmt->bindParam(":id", $this->id);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":breed_id", $this->breed_id);
        $stmt->bindParam(":users_id", $this->users_id);
        $stmt->bindParam(":sex", $this->sex);
        $stmt->bindParam(":color", $this->color);
        $stmt->bindParam(":birth", $this->birth);

        if ($stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }
    // метод для получения товаров
    function readAll($from_record_num, $records_per_page)
    {
        // запрос MySQL
        $query = "SELECT
                    id, name, breed_id, users_id, sex, color, birth
                FROM
                    " . $this->table_name . "
                ORDER BY
                    name ASC
                LIMIT
                    {$from_record_num}, {$records_per_page}";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }

    // используется для пагинации товаров
    public function countAll()
    {
        // запрос MySQL
        $query = "SELECT id FROM " . $this->table_name . "";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $num = $stmt->rowCount();

        return $num;
    }

        // метод для получения товара
    function readOne()
    {
        // запрос MySQL
        $query = "SELECT
                     id, name, breed_id, users_id, sex, color, birth
                FROM
                    " . $this->table_name . "
                WHERE
                    id = ?
                LIMIT
                    0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->id = $row["id"];
        $this->name = $row["name"];
        $this->breed_id = $row["breed_id"];
        $this->users_id = $row["users_id"];
        $this->sex = $row["sex"];
        $this->color = $row["color"];
        $this->birth = $row["birth"];
    }
    function update()
{
    // MySQL запрос для обновления записи (товара)
    $query = "UPDATE
                " . $this->table_name . "
            SET
                name = :name,
                breed_id = :breed_id,
                users_id = :users_id,
                sex = :sex,
                color = :color,
                birth  = :birth
            WHERE
                id = :id";

    // подготовка запроса
    $stmt = $this->conn->prepare($query);

    // очистка
    $this->name = htmlspecialchars(strip_tags($this->name));
    $this->breed_id = htmlspecialchars(strip_tags($this->breed_id));
    $this->users_id = htmlspecialchars(strip_tags($this->users_id));
    $this->sex = htmlspecialchars(strip_tags($this->sex));
    $this->color = htmlspecialchars(strip_tags($this->color));
    $this->birth = htmlspecialchars(strip_tags($this->birth));
    $this->id = htmlspecialchars(strip_tags($this->id));

    // привязка значений
    $stmt->bindParam(":name", $this->name);
    $stmt->bindParam(":breed_id", $this->breed_id);
    $stmt->bindParam(":users_id", $this->users_id);
    $stmt->bindParam(":sex", $this->sex);
    $stmt->bindParam(":color", $this->color);
    $stmt->bindParam(":birth", $this->birth);
    $stmt->bindParam(":id", $this->id);

    // выполняем запрос
    if ($stmt->execute()) {
        return true;
    }

    return false;
}
    // метод для удаления товара
    function delete()
    {
        // запрос MySQL для удаления
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        if ($result = $stmt->execute()) {
            return true;
        } else {
            return false;
        }
    }

        // выбираем котов по поисковому запросу
    public function search($search_term, $from_record_num, $records_per_page)
    {
        // запрос к БД
        $query = "SELECT
                c.cat_name as cat_name, p.id, p.title, p.price, p.origin, p.density, p.width , p.composition, p.cat_id
            FROM
                " . $this->table_name . " p
                LEFT JOIN
                    categories c
                        ON p.cat_id = c.id
            WHERE
                p.title LIKE ?
            ORDER BY
                p.title ASC
            LIMIT
                ?, ?";

        // подготавливаем запрос
        $stmt = $this->conn->prepare($query);

        // привязываем значения переменных
        $search_term = "%{$search_term}%";
        $stmt->bindParam(1, $search_term);
        // $stmt->bindParam(2, $search_term);
        $stmt->bindParam(2, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(3, $records_per_page, PDO::PARAM_INT);

        // выполняем запрос
        $stmt->execute();

        // возвращаем значения из БД
        return $stmt;
    }

    // метод для подсчёта общего количества строк
    public function countAll_BySearch($search_term)
    {
        // запрос
        $query = "SELECT
                COUNT(*) as total_rows
            FROM
                " . $this->table_name . " p 
            WHERE
                p.name LIKE ?";

        // подготовка запроса
        $stmt = $this->conn->prepare($query);

        // привязка значений
        $search_term = "%{$search_term}%";
        $stmt->bindParam(1, $search_term);
        // $stmt->bindParam(2, $search_term);

        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        return $row["total_rows"];
}
}