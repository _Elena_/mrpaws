<?php

class User
{
    // подключение к базе данных и имя таблицы
    private $conn;
    private $table_name = "users";

    // свойства объекта
    public $id;
    public $user_name;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    // данный метод используется в раскрывающемся списке
    function read()
    {
        // запрос MySQL: выбираем столбцы в таблице «categories»
        $query = "SELECT
                    id, user_name, user_email, user_pass, user_phone, user_country, user_city, user_info, user_image
                FROM
                    " . $this->table_name . "
                ORDER BY
                    user_name";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        return $stmt;
    }
    // получение названия категории по её ID
    function readName()
    {
        // запрос MySQL
        $query = "SELECT user_name FROM " . $this->table_name . " WHERE id = ? limit 0,1";

        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
        $stmt->execute();

        $row = $stmt->fetch(PDO::FETCH_ASSOC);

        $this->user_name = $row["user_name"];
    }
    
}