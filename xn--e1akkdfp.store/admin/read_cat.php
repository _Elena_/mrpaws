<?php
// получаем ID товара
$id = isset($_GET["id"]) ? $_GET["id"] : die("ERROR: отсутствует ID.");

// подключаем файлы для работы с базой данных и файлы с объектами
include_once "config/database.php";
include_once "objects/cats.php";
include_once "objects/users.php";
include_once "objects/breeds.php";

// получаем соединение с базой данных
$database = new Database();
$db = $database->getConnection();
 
// подготавливаем объекты
$cat = new Cat($db);
$user = new User($db);
$breed = new Breed($db);
// устанавливаем свойство ID товара для чтения
$cat->id = $id;

// получаем информацию о товаре
$cat->readOne();
// установка заголовка страницы
$page_title = "Страница кота (чтение одного кота)";

require_once "header.php";
?>

<!-- ссылка на все товары -->
<div class="right-button-margin">
    <a href="index.php" class="btn btn-primary pull-right">
        <span class="glyphicon glyphicon-list"></span> Просмотр всех котов
    </a>
</div>
<!-- HTML-таблица для отображения информации о товаре -->
<table class="table table-hover table-responsive table-bordered">
    <tr>
        <td>Имя</td>
        <td><?= $cat->name; ?></td>
    </tr>
    <tr>
        <td>Порода</td>
        <td>
        <?php 
            $breed->id = $cat->breed_id; 
            $breed->readName(); 
            echo $breed->breed_name; 
            echo " (" . $breed ->id . ")";
        ?>
        </td>
    </tr>

    <tr>
        <td>Владелец</td>
        <td>
        <?php 
            $user->id = $cat->users_id; 
            $user->readName(); 
            echo $user->user_name; 
            echo " (" . $user ->id . ")";
        ?>
        </td>
    </tr>
    <tr>
        <td>Цвет</td>
        <td><?= $cat->color; ?></td>
    </tr>
        <tr>
        <td>Возраст</td>
        <td><?= $cat->birth; ?></td>
    </tr>
    <tr>
        <td>Пол</td>
        <td><?= $cat->sex; ?></td>
    </tr>
</table>
<script>
    // JavaScript для удаления товара
    $(document).on("click", ".delete-object", function() {
        const id = $(this).attr("delete-id");

        bootbox.confirm({
            message: "<h4>Вы уверены?</h4>",
            buttons: {
                confirm: {
                    label: "<span class='glyphicon glyphicon-ok'></span> Да",
                    className: "btn-danger"
                },
                cancel: {
                    label: "<span class='glyphicon glyphicon-remove'></span> Нет",
                    className: "btn-primary"
                }
            },
            callback: function(result) {
                if (result == true) {
                    $.post("delete_cat.php", {
                        object_id: id
                    }, function(data) {
                        location.reload();
                    }).fail(function() {
                        alert("Невозможно удалить.");
                    });
                }
            }
        });

        return false;
    });
</script>
<?php // подвал
require_once "footer.php";