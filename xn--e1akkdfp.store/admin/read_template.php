<?php

// форма поиска
echo "<form role='search' action='search.php'>";
echo "<div class='input-group col-md-3 pull-left margin-right-1em'>";
$search_value = isset($search_term) ? "value='{$search_term}'" : "";
echo "<input type='text' class='form-control' placeholder='Введите название или описание кота ...' name='s' required {$search_value} />";
echo "<div class='input-group-btn'>";
echo "<button class='btn btn-primary' type='submit'><i class='glyphicon glyphicon-search'></i></button>";
echo "</div>";
echo "</div>";
echo "</form>";

// кнопка создания товара
echo "<div class='right-button-margin'>";
echo "<a href='create_cat.php' class='btn btn-primary pull-right'>";
echo "<span class='glyphicon glyphicon-plus'></span> Создать кота";
echo "</a>";
echo "</div>";

// показать товары, если они есть
if ($total_rows > 0) {

    echo "<table class='table table-hover table-responsive table-bordered'>";
    echo "<tr>";
    echo "<th>Имя</th>";
    echo "<th>Порода</th>";
    echo "<th>Владелец</th>";
    echo "<th>Цвет</th>";
    echo "<th>Возраст</th>";
    echo "<th>Пол</th>";
    echo "<th>Действия</th>";
    echo "</tr>";

    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {

        extract($row);

        echo "<tr>";
        echo "<td>{$name}</td>";
        
        echo "<td>";
        $breed->id = $breed_id;
        $breed->readName();
        echo $breed->breed_name;
        echo "</td>";
        
        echo "<td>";
        $user->id = $users_id;
        $user->readName();
        echo $user->user_name;
        echo " ({$users_id})";
        echo "</td>";
        echo "<td>{$color}</td>";
        echo "<td>{$birth}</td>";
        echo "<td>{$sex}</td>";
        echo "<td>";

        // кнопка просмотра товара
        echo "<a href='read_cat.php?id={$id}' class='btn btn-primary left-margin'>";
        echo "<span class='glyphicon glyphicon-list'></span> Просмотр";
        echo "</a>";

        // кнопка редактирования товара
        echo "<a href='update_cat.php?id={$id}' class='btn btn-info left-margin'>";
        echo "<span class='glyphicon glyphicon-edit'></span> Редактировать";
        echo "</a>";

        // кнопка удаления товара
        echo "<form style='display:inline;' method='POST' action='delete_cat.php'>";
        echo "<button type='submit' name='delete_id' value='{$id}' class='btn btn-danger delete-object'>";
        echo "<span class='glyphicon glyphicon-remove'></span> Удалить";
        echo "</button>";
        echo "</form>";

        echo "</td>";

        echo "</tr>";
    }

    echo "</table>";

    // пагинация
    include_once "paging.php";
}

// сообщить пользователю, что товаров нет
else {
    echo "<div class='alert alert-danger'>Котов не найдено.</div>";
}
