<?php
// подключаем файлы для работы с базой данных и файлы с объектами
include_once "config/database.php";
include_once "objects/cats.php";
include_once "objects/breeds.php";
include_once "objects/users.php";
// установка заголовка страницы
$page_title = "Обновление кота";


// получаем ID редактируемого кота
$id = isset($_GET["id"]) ? $_GET["id"] : die("ERROR: отсутствует ID.");
// получаем соединение с базой данных
$database = new Database();
$db = $database->getConnection();

// подготавливаем объекты
$cat = new Cat($db);
$user = new User($db);
$breed = new Breed($db);
// устанавливаем свойство ID кота для редактирования
$cat->id = $id;

// получаем информацию о редактируемом коте
$cat->readOne();

include_once "header.php";
?>

<div class="right-button-margin">
    <a href="index.php" class="btn btn-default pull-right">Просмотр всех котов</a>
</div>
        <?php
        // если форма была отправлена (submit)
        if ($_POST) {

            // устанавливаем значения свойствам кота
            $cat->name = $_POST['name'];
            $cat->breed_id = $_POST['breed'];
            $cat->users_id = $_POST['user'];
            $cat->color = $_POST['color'];
            $cat->birth = $_POST['birth'];
            $cat->sex = $_POST['sex'];

            // обновление товара
            if ($product->update()) {
                echo "<div class='alert alert-success alert-dismissable'>";
                echo "Кот был обновлён.";
                echo "</div>";
            }

            // если не удается обновить товар, сообщим об этом пользователю
            else {
                echo "<div class='alert alert-danger alert-dismissable'>";
                echo "Невозможно обновить кота.";
                echo "</div>";
            }
        }
        ?>

<form action="<?= htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}"); ?>" method="post">
    <table class="table table-hover table-responsive table-bordered">

        <tr>
            <td>Имя</td>
            <td><input type="text" name="name" value="<?= $cat->name; ?>" class="form-control" /></td>
        </tr>

        <tr>
            <td>Владелец</td>
            <td>
            <?php
            // читаем породы базы данных
            $stmt = $user->read();
            
            // помещаем их в выпадающий список
            echo "<select class='form-control' name='user'>";
            echo "<option>Выбрать владельца...</option>";
            
            while ($users = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($users);
                echo "<option value='{$id}'>{$user_name} ({$id})</option>";
            }
            
            echo "</select>";
            ?>
            </td>
        </tr>
        <tr>
            <td>Порода</td>
            <td>
            <?php
            // читаем породы базы данных
            $stmt = $breed->read();
            
            // помещаем их в выпадающий список
            echo "<select class='form-control' name='breed'>";
            echo "<option>Выбрать породу...</option>";
            
            while ($breeds = $stmt->fetch(PDO::FETCH_ASSOC)) {
                extract($breeds);
                echo "<option value='{$breed_id}'>{$breed_name}</option>";
            }
            echo "</select>";
            ?>
            </td>
        </tr>
        <tr>
            <td>Окрас</td>
            <td><input type="text" name="color" class="form-control" value="<?= $cat->color; ?>"/></td>
        </tr>
        <tr>
            <td>Возраст</td>
            <td><input type="text" name="birth" class="form-control" value="<?= $cat->birth; ?>"/></td>
        </tr>
        <tr>
            <td>Пол</td>
            <td><input type="text" name="sex" class="form-control" value="<?= $cat->sex; ?>"/></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Обновить</button>
            </td>
        </tr>
    </table>
</form>
<?php




require_once "footer.php";
?>