<?php
require 'header.php';
?>
<main class="main">
    <section class="cat container">
<?php
if(isset($_GET['breed']) && !empty($_GET['breed'])){
    $breed_id = $_GET['breed'];
    $name = "";
    $shortname = "";
    $group = "";
    $category = "";
    $origin = "";
    $history = "";
    $image = "";
    $query = "SELECT * FROM `breed` WHERE `breed_id`='$breed_id';";
    $result = mysqli_query($connection, $query);
    while($row = mysqli_fetch_assoc($result)){
        $name = $row['breed_name'];
        $shortname = $row['breed_shortname'];
        $group = $row['breed_group'];
        $category = $row['breed_category'];
        $origin = $row['breed_origin'];
        $history = $row['breed_history'];
        $image = $row['breed_image'];
    }
?>
        <div class="breed-inner">
            <div class="breed__info">
                <div class="breed__info-part">
                    <div class="breed__info-img" style="overflow:hidden;">
                    <img src="/assets/img/breed/<?php echo $image; ?>" alt="Кот" style="object-fit:cover;object-position:center;width:100%;height:100%;">
                </div>
                </div>
                <div class="breed__info-part">
                    <div class="breed__info-name"> <span><?php echo $name; ?></span></div>
                    <div class="cat__info-about">
                        <p><strong>Обозначение: </strong> <?php echo $shortname; ?></p>
                        <p><strong>Группа: </strong> <?php echo $group; ?></p>
                        <p><strong>Категория: </strong> <?php echo $category; ?></p>
                        <p><strong>Происождение: </strong> <?php echo $origin; ?></p>
                    </div>
                </div>
            </div>
                <h2 class="section-title">История породы</h2>
                <div class="cat__info-about">
                    <p><?php echo $history; ?></p>
                </div>
        </div>
        <?php
        } else {
            echo "<p>Похоже, такой записи нет.</p>";
        }
        ?>
    </section>
</main>
<?php
require 'footer.php';
?>