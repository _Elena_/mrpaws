<?php
require 'header.php'
?>
<main class="main">
  <section class="breeds">
    <h2 class="section-title">Список пород кошек:</h2>
    <div class="breeds-inner container">
      <ul class="breeds__list" id="paginated-list" data-current-page="1">
        <?php
        $query = 'SELECT * FROM breed;';
        $result = mysqli_query($connection, $query);
            while($breedRow = mysqli_fetch_assoc($result)){
        ?>
        <li class="breeds__list-item">
          <div class="breeds-img" style="overflow:hidden;"><img style="width:100%;height:100%;object-fit:cover;" src="assets/img/breed/<?php echo $breedRow['breed_image']; ?>" alt="#"></div>
          <a href="breed.php?breed=<?php echo $breedRow['breed_id']; ?>" class="breeds__info">
            <h3 class="breeds__info-title"><?php echo $breedRow['breed_name']; ?></h3>
            <p>Обозначение: <?php echo $breedRow['breed_shortname']; ?></p>
            <p>Группа: <?php echo $breedRow['breed_group']; ?></p>
            <p>Категория: <?php echo $breedRow['breed_category']; ?></p>
            <p>Происхождение: <?php echo $breedRow['breed_origin']; ?></p>
          </a>
        </li>
        <?
        }
        ?>
      </ul>
    </div>
    <div class="pagination-container">
      <button class="pagination-button" id="prev-button">&lt;</button>
      <div id="pagination-numbers"></div>
      <button class="pagination-button" id="next-button">&gt;</button>
    </div>
  </section><script src="assets/js/breeds.js"></script>
</main>
<?php
require 'footer.php'
?>