<?php
require 'header.php'
?>
<main class="main">
  <section class="cat container">
    <div class="cat-inner">
          <?php
          if(isset($_GET['cat'])){
            $catId = $_GET['cat'];
            $query = "SELECT * FROM cats WHERE id LIKE '$catId';";
            $result = mysqli_query($connection, $query);
            while($catsRow = mysqli_fetch_assoc($result)){
              $usersId = $catsRow['users_id'];
              $user_query = "SELECT * FROM users WHERE id LIKE '$usersId';";
              $user_result = mysqli_query($connection, $user_query);
              
              $cat_breed = $catsRow['breed_id'];

              $this_breed_name = '';
              $this_breed_id = '';
              $breed_query = "SELECT breed_name FROM breed WHERE breed_id LIKE '$cat_breed'";
              $breed_result = mysqli_query($connection, $breed_query);
              while($breedRow = mysqli_fetch_assoc($breed_result)){
                $this_breed_name = $breedRow['breed_name'];
                $this_breed_id = $breedRow['breed_id'];
              }
          ?>
      <div class="cat__info">
        <div class="cat__info-part">
          <div class="cat__info-img"><img src="assets/img/cats/<?php echo $catsRow['picture']; ?>" alt="#"></div>
        </div>
        <div class="cat__info-part">
          <div class="cat__info-name"> <span><?php echo $catsRow['name']; ?></span></div>
          <div class="cat__info-about">
            <p><strong>Пол: </strong><?php echo $catsRow['sex']; ?></p>
            <p><strong>Порода: </strong><a href="breed.php?=<?php echo $this_breed_id; ?>"><?php echo $this_breed_name; ?></a></p>
            <p><strong>Окрас: </strong><?php echo $catsRow['color']; ?></p>
            <p>
              <strong>Возраст: </strong>
              <?php echo $catsRow['birth']; 
              if( intval(substr($catsRow['birth'], -1)) == 1){
                echo ' год';
              } else if(intval(substr($catsRow['birth'], -1)) > 1 && intval(substr($catsRow['birth'], -1)) < 5){
                echo ' года';
              } else if(intval(substr($catsRow['birth'], -1)) > 4 && intval(substr($catsRow['birth'], -1)) <= 9){
                echo ' лет';
              }
              ?>
            </p>
          <?php while($userRow = mysqli_fetch_assoc($user_result)){ ?>
            <p><strong>Владелец: </strong><?php echo $userRow['user_name']; ?></p>
          </div>
          <div class="cat__info-contacts">
            <a href="profile.php?user=<?php echo $userRow['id']; ?>">Связаться с владельцем</a>
          </div>
            <?php
              }
          }
        }
          ?>
        </div>
      </div>
    </div>
  </section>
</main>
<?php
require 'footer.php'
?>