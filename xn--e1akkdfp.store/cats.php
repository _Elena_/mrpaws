<?php
require 'header.php'
?>
<main class="main">
  <section class="cats">
    <h2 class="section-title">Кошки наших пользователей:</h2>
    <div class="cats-inner container">
      <ul class="cats__list" id="paginated-list" data-current-page="1">
        <?php
        $query = "SELECT * FROM cats;";
        $result = mysqli_query($connection, $query);
            while($catsRow = mysqli_fetch_assoc($result)){
        ?>
        <li class="cats__list-item"> 
          <div class="cats-img" style="overflow:hidden;margin:0 auto;"><img style="width:100%;height:100%;object-fit:cover;" src="assets/img/cats/<?php echo $catsRow['picture']; ?>" alt="#"></div>
          <a href="cat.php?cat=<?php echo $catsRow['id']; ?>" class="cats__info">
            <h3 class="cats__info-title"><?php echo $catsRow['name']; ?></h3>
            <?php
              $usersId = $catsRow['users_id'];
              $user_query = "SELECT user_name FROM users WHERE id LIKE '$usersId';";
              $user_result = mysqli_query($connection, $user_query);
              while($userRow = mysqli_fetch_assoc($user_result)){ ?>
            <p>Владелец: <?php echo $userRow['user_name']; ?></p>
            <?php
              }
            ?>
          </a>
        </li>
        <?
        }
        ?>
      </ul>
    </div>
    <div class="pagination-container">
      <button class="pagination-button" id="prev-button">&lt;</button>
      <div id="pagination-numbers"></div>
      <button class="pagination-button" id="next-button">&gt;</button>
    </div>
  </section>
  <script src="assets/js/cats.js"></script>
</main>
<?php
require 'footer.php'
?>