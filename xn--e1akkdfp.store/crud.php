<?php
require 'db.php';
define('KB', 1024);
define('MB', 1048576);
if(!$connection){
    die("Connection failed: " . mysqli_connect_error());
}

if(isset($_POST['update_user'])){
    $name = trim($_POST['user_name']);
    $country = trim($_POST['user_country']);
    $city = trim($_POST['user_city']);
    $info = trim($_POST['user_info']);
    $phone = trim($_POST['user_phone']);
    $image = trim($_POST['user_image']);
    $id = trim($_POST['user_id']);
    $query = '';
    if(isset($_FILES['user_image']) && !empty($_FILES['user_image']) && $_FILES['user_image']['name'] != ""){
        $upload_destination = 'assets/img/users/';
        if ($_FILES['user_image']['size'] > 2*MB){
            exit('Please upload a file less than 2MB!');
        }
        $filename = md5(uniqid()) . '.' . end(explode('.', $_FILES['user_image']['name']));
        if(strlen($filename) > 20){
            $filename = substr($filename, 0, 20).'.'.end(explode('.', $filename));
        }
        $file = $upload_destination . $filename;
        move_uploaded_file($_FILES['user_image']['tmp_name'], $file);
        $image = $filename;
    }

    $query = "UPDATE `users` SET ";
    if(!empty($name)){
        $query = $query."`user_name`='$name',";
    }
    if(!empty($country)){
        $query = $query."`user_country`='$country',";
    }
    if(!empty($city)){
        $query = $query."`user_city`='$city',";
    }
    if(!empty($phone)){
        $query = $query."`user_phone`='$phone',";
    }
    if(!empty($info)){
        $query = $query."`user_info`='$info',";
    }
    if(isset($_FILES['user_image']) && !empty($image) && $image != ""){
        $query = $query."`user_image`='$image' ";
    }
    $query = $query."WHERE `id`='$id';";
    if(strpos($query, ',WHERE')){
        $query = str_replace(',WHERE', ' WHERE', $query);
    }

    if(mysqli_query($connection, $query)) {
        header('Location: account.php?status=green&updid=1#updateuser');
        echo 'User information updated!';
        mysqli_close($connection);
    } else {
        header('Location: account.php?status=red&updid=1#updateuser');
        echo "Error updating user information: " . mysqli_error($connection);
        mysqli_close($connection);
    }
}

if(isset($_POST['update_pass'])){
    $id = trim($_POST['user_id']);
    $newpass = trim($_POST['user_pass']);
    $oldpass = trim($_POST['user_pass_old']);
    if((isset($newpass) && !empty($newpass)) && (isset($oldpass) && !empty($oldpass))){
        $pass_confirm = "SELECT `user_pass` FROM `users` WHERE `id`='$id';";
        $pass_result = mysqli_query($connection, $pass_confirm);
        $oldpass_stored = '';
        while($passRow = mysqli_fetch_assoc($pass_result)){
            $oldpass_stored = $passRow['user_pass'];
        }
        if($oldpass_stored === $oldpass && $oldpass_stored != $newpass){
            $query = "UPDATE `users` SET `user_pass`='$newpass' WHERE `id`='$id'";
            if(mysqli_query($connection, $query)) {
                echo 'User password updated!';
                mysqli_close($connection);
                header('Location: account.php?status=green&updid=3#updatepass');
            } else {
                echo "Error updating password: " . mysqli_error($connection);
                mysqli_close($connection);
                header('Location: account.php?status=red&updid=3#updatepass');
            }
        } else{
            mysqli_close($connection);
            header('Location: account.php?status=yellow&updid=3#updatepass');
        }
    }
}

if(isset($_POST['update_email'])){
    if(isset($_POST['user_email']) && !empty($_POST['user_email'])){
        $id = trim($_POST['user_id']);
        $email = trim($_POST['user_email']);
        $oldpass = trim($_POST['user_pass_old']);
        $pass_confirm = "SELECT `user_pass` FROM `users` WHERE `id`='$id';";
        $pass_result = mysqli_query($connection, $pass_confirm);
        $oldpass_stored = '';
        $email_stored = '';
        while($passRow = mysqli_fetch_assoc($pass_result)){
            $oldpass_stored = $passRow['user_pass'];
            $email_stored = $passRow['user_email'];
        }
        if($oldpass_stored === $oldpass && $email_stored != $email){
            $query = "UPDATE `users` SET `user_email`='$email' WHERE `id`='$id'";
            if(mysqli_query($connection, $query)) {
                echo 'User email updated!';
                $_SESSION['email'] = $email;
                setcookie('user', $email, time()+3600);
                header('Location: account.php?status=green&updid=2#updateemail');
                mysqli_close($connection);
            } else {
                echo "Error updating email: " . mysqli_error($connection);
                mysqli_close($connection);
                header('Location: account.php?status=red&updid=2#updateemail');
            }
        } else{
            mysqli_close($connection);
            header('Location: account.php?status=yellow&updid=2#updateemail');
        }
    }
}

if(isset($_POST['update_cat']) || isset($_POST['create_cat'])){
    $name = trim($_POST['cat_name']);
    $age = intval(trim($_POST['cat_age']));
    $breed = intval(trim($_POST['cat_breed']));
    $color = trim($_POST['cat_color']);
    $sex = trim($_POST['cat_sex']);
    $image = trim($_POST['cat_image']);
    $cat = intval(trim($_POST['cat_id']));
    $user = intval(trim($_POST['user_id']));

    if(isset($_FILES['cat_image']) && !empty($_FILES['cat_image']) && $_FILES['cat_image']['name'] != ""){
        $upload_destination = 'assets/img/cats/';
        if ($_FILES['cat_image']['size'] > 2*MB){
            exit('Please upload a file less than 2MB!');
        }
        $filename = md5(uniqid()) . '.' . end(explode('.', $_FILES['cat_image']['name']));
        if(strlen($filename) > 20){
            $filename = substr($filename, 0, 20).'.'.end(explode('.', $filename));
        }
        $file = $upload_destination . $filename;
        move_uploaded_file($_FILES['cat_image']['tmp_name'], $file);
        $image = $filename;
    }
    $query = '';
    if(isset($_POST['create_cat'])){
        if(!empty($image)){
            $query = "INSERT INTO `cats` (`name`, `breed_id`, `users_id`, `sex`, `color`, `birth`, `picture`) VALUES ('$name', '$breed', '$user', '$sex', '$color', '$age', '$image');";
        } else{
            $query = "INSERT INTO `cats` (`name`, `breed_id`, `users_id`, `sex`, `color`, `birth`) VALUES ('$name', '$breed', '$user', '$sex', '$color', '$age');";
        }
    } else if(isset($_POST['update_cat'])){
        $query = "UPDATE `cats` SET ";
        if(!empty($name)){
            $query = $query."`name`='$name',";
        }
        if(!empty($breed)){
            $query = $query."`breed_id`=$breed,";
        }
        if(!empty($sex)){
            $query = $query."`sex`='$sex',";
        }
        if(!empty($color)){
            $query = $query."`color`='$color',";
        }
        if(!empty($age)){
            $query = $query."`birth`=$age,";
        }
        if(isset($_FILES['cat_image']) && !empty($image) && $image != ""){
            $query = $query."`picture`='$image' ";
        }
        $query = $query."WHERE `id`=$cat AND `users_id`=$user;";
        if(strpos($query, ',WHERE')){
            $query = str_replace(',WHERE', ' WHERE', $query);
        }
    }

    if(mysqli_query($connection, $query)) {
        echo 'Cat record updated!';
        mysqli_close($connection);
        header('Location: profile.php');
    } else {
        echo "Error updating record: " . mysqli_error($connection);
        mysqli_close($connection);
    }
}

if(isset($_POST['delete_cat'])){
    $cat_to_del = intval($_POST['cat_id_to_del']);
    $user_to_del = intval($_POST['user_id_to_del']);
    $del_query = "DELETE FROM `cats` WHERE `id`='$cat_to_del' AND `users_id`='$user_to_del'";
    if(mysqli_query($connection, $del_query)) {
        echo 'Cat record deleted!';
        header('Location: account.php?');
        mysqli_close($connection);
    } else {
        echo "Error deleting record: " . mysqli_error($connection);
        mysqli_close($connection);
        header('Location: account.php?');
    }
}
?>