<?php
require 'header.php';
if(isset($_GET['cat']) && !empty($_COOKIE['user'])){
    $loggedUser = $_COOKIE['user'];
    $user_query = "SELECT * FROM users WHERE user_email LIKE '$loggedUser';";
    $userId = "";
    $u_name = '';
    $u_country = '';
    $u_city = '';
    $u_info = '';
    $u_email = '';
    $u_pass = '';
    $u_image = '';

    $user_result = mysqli_query($connection, $user_query);
    while($userRow = mysqli_fetch_assoc($user_result)){
        $userId = $userRow['id'];
        $u_name = $userRow['user_name'];
        $u_country = $userRow['user_country'];
        $u_city = $userRow['user_city'];
        $u_info = $userRow['user_info'];
        $u_email = $userRow['user_email'];
        $u_pass = $userRow['user_pass'];
        $u_image = $userRow['user_image'];
    }
    ?>
<main class="main">
    <?php
    $catId = $_GET['cat'];
    $query = "SELECT * FROM cats WHERE id LIKE '$catId' AND users_id LIKE '$userId';";
    $result = mysqli_query($connection, $query);
    while($editRow = mysqli_fetch_assoc($result)){
        $thisBreed = $editRow['breed_id'];
    ?>
    <style>
        .update-input{
            border-bottom: 1px solid black;
            padding-bottom: 16px;
            font-size: 22px;
        }
        .update-input input, .update-input select{
            border: none;
            padding: 8px 10px;
            font-size: 22px;
        }
        .update-input div{
            display: grid;
            grid-template-columns: repeat(4, 1fr);
            align-items: center;
            color: white;
            background-color: #4b847d;
            padding: 10px 12px;
        }
        button[type="submit"]{
            display: block;
            margin: 24px auto 0;
            font-size: 42px;
            background-color: #996494;
            border: none;
            border-radius: 0;
            color: white;
            padding: 19px 24px;
        }
    </style>
    <section class="cat container">
        <h2>Обновить запись</h2>
        <h3>Оставьте поле пустым, если вы не хотите менять часть записи</h3>
        <form action="crud.php" method="POST" enctype=multipart/form-data>
            <div class="update-input">
                <p><strong>Имя</strong>: <?php echo $editRow['name']; ?></p>
                <div>
                    <label for="c_name">Сменить на: </label>
                    <input type="text" id="c_name" name="cat_name">
                </div>
            </div>
            <div class="update-input">
                <p><strong>Возраст</strong>: <?php echo $editRow['birth']; ?></p>
                <div>
                    <label for="c_age">Сменить на: </label>
                    <input type="text" id="c_age" name="cat_age">
                </div>
            </div>
            <div class="update-input">
                <?php
                $breed_query = "SELECT breed_name FROM breed WHERE breed_id LIKE '$thisBreed';";
                $breed_result = mysqli_query($connection, $breed_query);
                while($breedRow = mysqli_fetch_assoc($breed_result)){
                ?>
                <p><strong>Порода</strong>: <?php echo $breedRow['breed_name']; ?></p>
                <?php
                }
                ?>
                <div>
                    <label for="c_breed">Сменить на: </label>
                    <select id="c_breed" name="cat_breed">
                        <?php
                        $breeds_query = "SELECT * FROM breed";
                        $breeds_result = mysqli_query($connection, $breeds_query);
                        while($breedsRow = mysqli_fetch_assoc($breeds_result)){
                            if($breedsRow['breed_id'] != $thisBreed){
                                ?>
                                    <option value="<?php echo $breedsRow['breed_id']; ?>"><?php echo $breedsRow['breed_name']; ?></option>
                                    <?php
                            } else{
                                ?>
                                    <option selected value="<?php echo $breedsRow['breed_id']; ?>"><?php echo $breedsRow['breed_name']; ?></option>
                                    <?php
                            }
                            ?>  
                            <?php
                            }
                            ?>
                    </select>
                </div>
            </div>
            <div class="update-input">
                <p><strong>Окрас</strong>: <?php echo $editRow['color']; ?></p>
                <div>
                    <label for="c_color">Сменить на: </label>
                    <input type="text" id="c_color" name="cat_color">
                </div>
            </div>
            <div class="update-input">
                <p><strong>Пол</strong>: <?php echo $editRow['sex']; ?></p>
                <div>
                    <label for="c_sex">Сменить на: </label>
                    <select id="c_sex" name="cat_sex">
                        <?php
                            if($editRow['sex'] == 'мужской'){
                        ?>
                            <option value="мужской">не менять</option>
                            <option value="женский">женский</option>
                        <?php
                            } else{
                        ?>
                            <option value="женский">не менять</option>
                            <option value="мужской">мужской</option>
                        <?php   
                            }
                        ?>
                    </select>
                </div>
            </div>
            <div class="update-input">
                <p>Фотография: </p>
                <div>
                    <label for="c_image">Загрузить изображение: </label>
                    <input id="c_image" name="cat_image" type="file" accept="image/*">
                </div>
            </div>
            <div style="display:none;">
                <input type="text" name="cat_id" value="<?php echo $catId; ?>">
                <input type="text" name="user_id" value="<?php echo $userId; ?>">
            </div>
            <div>
                <button type="submit" value="update" name="update_cat">Обновить запись</button>
            </div>
        </form>
    </section>
</main>
<?php
    }
}
require 'footer.php';
?>