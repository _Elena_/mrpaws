<footer class="footer">
    <div class="footer-inner container">
      <div class="footer__item"><span class="footer__list-heading">Разделы</span>
        <ul class="footer__list">
          <li class="footer__list-item"> <a href="structural.php">Главная</a></li>
          <li class="footer__list-item"> <a href="breeds.php">Породы</a></li>
          <li class="footer__list-item"> <a href="cats.php">Наши кошки</a></li>
          <li class="footer__list-item"> <a href="profile.php">Профиль</a></li>
        </ul>
      </div>
      <div class="footer__item"><span class="footer__list-heading">Информация</span>
        <ul class="footer__list">
          <li class="footer__list-item"> <a href="#">О сайте</a></li>
          <li class="footer__list-item"> <a href="#">Заводчику</a></li>
          <li class="footer__list-item"> <a href="#">Помощь</a></li>
          <li class="footer__list-item"> <a href="#">Мои настройки</a></li>
        </ul>
      </div>
      <div class="footer__item"><span class="footer__list-heading">Контакты</span>
        <ul class="footer__list">
          <li class="footer__list-item"> <span>Телефон: </span><a href="#">8-910-020-30-40</a></li>
          <li class="footer__list-item"> <span>Почта: </span><a href="#">adress@pochta.com</a></li>
        </ul>
      </div>
    </div>
  </footer></body>
  </html>