<?php
require 'header.php';
if(isset($_GET['wrid']) && $_GET['wrid'] == '1'){
  if($_GET['status']=="green"){
      echo '<p class="sysmessage" style="background-color: green;color:white;margin:0;padding: 8px 12px;text-align:center;">Вы успешно вошли.</p>';
  }
}
?>

<main class="main">
  <section class="activity">
    <div class="container">
      <h2 class="section-title">Деятельность клуба</h2>
      <div class="activity-inner">
        <ul class="activity__list">
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-1.svg" alt="Зелёный клубок"></div>
            <p>Регистрируем животных</p>
          </li>
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-2.svg" alt="Оранжевый клубок"></div>
            <p>Проводим выставки</p>
          </li>
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-3.svg" alt="Фиолетовый клубок"></div>
            <p>Консультируем владельцев</p>
          </li>
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-3.svg" alt="Фиолетовый клубок"></div>
            <p>Фелинология</p>
          </li>
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-2.svg" alt="Оранжевый клубок"></div>
            <p>Представительство</p>
          </li>
          <li class="activity__list-item"> 
            <div class="activity-img"><img src="../assets/img/clew-1.svg" alt="Зелёный клубок"></div>
            <p>Поддержка сообщества</p>
          </li>
        </ul>
      </div>
    </div>
  </section>
  <section class="news container">
    <h2 class="section-title">Новости</h2>
    <div class="news-inner">
      <?php
      $query = 'SELECT * FROM `news` ORDER BY `id` DESC LIMIT 3;';
      $result = mysqli_query($connection, $query);
        while($newsRow = mysqli_fetch_assoc($result)){
      ?>
      <a class="news__item" href="article.php?article=<?php echo $newsRow['id']; ?>">
        <div class="news__item-top" style="background-image:url('assets/img/news/<?php echo $newsRow['picture']; ?>');background-size:cover;background-position:center;background-repeat:norepeat;";></div>
        <div class="news__item-bottom" style="padding:2rem;"><h3 style="margin:0;"><?php echo $newsRow['title']; ?></h3></div>
      </a>
      <?
          $count++;
        }
      ?>
    </div>
  </section>
  <?php 
  if(empty($_COOKIE['user'])){
  ?>
  <section class="register" id="register"> 
    <?php
        if(isset($_GET['wrid']) && $_GET['wrid'] == '1'){
            if($_GET['status']=="yellow"){
                echo '<p class="sysmessage" style="background-color: yellow;">Пользователь с такой почтой уже существует. Попробуйте ещё раз.</p>';
            } 
        }
        if(isset($_GET['rdid']) && $_GET['rdid'] == '1'){
          if($_GET['status']=="yellow"){
              echo '<p class="sysmessage" style="background-color: yellow;">Неверная связка почта/пароль.</p>';
          } 
      }
    ?>
    <div class="container">
      <div class="register-inner">
        <form class="register-block" action='header.php' method="POST">
          <h2 class="section-title">Регистрация</h2>
          <span style="display:block;text-align:center;">Для входа введите почту и пароль</span>
          <div class="register-inputs">
            <div class="register-input">
              <label for="name">Ваше имя:</label>
              <input type="text" name='username' id="name">
            </div>
            <div class="register-input">
              <label for="email">Email:</label>
              <input type="email" name='email' id="email" required>
            </div>
            <div class="register-input">
              <label for="tel">Телефон:</label>
              <input type="tel" name='tel' id="tel">
            </div>
            <div class="register-input">
              <label for="pass">Пароль:</label>
              <input type="password" name='password' id="pass" required>
            </div>
          </div>
          <button type="submit" name="submit">Вступить в клуб</button>
        </form>
      </div>
    </div>
  </section>
  <?php
  }
  ?>
</main>
<?php
require 'footer.php'
?>