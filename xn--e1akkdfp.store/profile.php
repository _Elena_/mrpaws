<?php
require 'header.php';
?>
<?php
  if(!empty($_COOKIE['user']) || !empty($_GET['user'])){
    $thisQuery = "";
    $query = "";
    $visitingUser = "";
    $loggedUser = "";
    $condition = "";
    if(!empty($_GET['user']) && empty($_COOKIE['user'])){
      $visitingUser = $_GET['user'];
      $thisQuery = "SELECT * FROM users WHERE id LIKE '$visitingUser';";
      $query = $thisQuery;
    } else if( (empty($_GET['user']) && !empty($_COOKIE['user'])) ){
      $loggedUser = $_COOKIE['user'];
      $thisQuery = "SELECT * FROM users WHERE user_email LIKE '$loggedUser';";
      $query = $thisQuery;
    } else if(!empty($_GET['user']) && !empty($_COOKIE['user'])){
      $loggedUser = $_COOKIE['user'];
      $thisQuery = "SELECT * FROM users WHERE user_email LIKE '$loggedUser';";
      $mid_result = mysqli_query($connection, $thisQuery);
      while($defineUserRow = mysqli_fetch_assoc($mid_result)){
        if($defineUserRow['id'] == $_GET['user']){
          $query = "SELECT * FROM users WHERE user_email LIKE '$loggedUser';";
        } else{
          $otherUser = $_GET['user'];
          $query = "SELECT * FROM users WHERE id LIKE '$otherUser';";
        }
      }
    }
    $userId = null;
    $result = mysqli_query($connection, $query);
    while($userRow = mysqli_fetch_assoc($result)){
      $userId = $userRow['id'];
?>
<main class="main">
  <section class="profile container">
    <div class="profile-inner">
      <div class="profile__info">
        <div class="profile__info-part">
          <div class="profile__info-img"><img src="assets/img/users/<?php echo $userRow['user_image']; ?>" alt="#"></div>
        </div>
        <div class="profile__info-part">
          <div class="profile__info-name"> <span><?php echo $userRow['user_name']; ?></span>
          <?php
          if( !empty($loggedUser) && $userRow['user_email'] == $loggedUser ){
          ?>
          <a href="account.php" style="font-size:14px;color:#4b847d;">Редактировать профиль</a></div>
          <?php
          }
          ?>
          <div class="profile__info-location">
            <svg width="21" height="32" viewBox="0 0 21 32" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M10.3201 31.2C8.93008 29 7.54008 26.77 6.16008 24.55C4.0903 21.4382 2.34136 18.1246 0.940083 14.66C-0.102955 12.2383 -0.262226 9.52716 0.490083 6.99999C1.26691 4.91175 2.67339 3.11648 4.51502 1.86245C6.35666 0.608418 8.54244 -0.042402 10.7701 -5.87758e-06C12.9513 0.0881099 15.0537 0.840065 16.796 2.15531C18.5384 3.47055 19.8376 5.28638 20.5201 7.35999C21.1365 9.76252 20.954 12.3004 20.0001 14.59C18.7566 17.7443 17.1882 20.7605 15.3201 23.59C13.8701 25.94 12.3801 28.27 10.9101 30.59C10.8001 30.77 10.7001 30.97 10.6001 31.16L10.3201 31.2ZM14.3201 10.45C14.3025 9.44394 13.8977 8.4834 13.1899 7.76819C12.4821 7.05298 11.5259 6.63815 10.5201 6.60999C9.50374 6.62018 8.5313 7.02573 7.80888 7.74069C7.08646 8.45564 6.67083 9.42381 6.65008 10.44C6.6677 11.446 7.0725 12.4066 7.78026 13.1218C8.48802 13.837 9.44427 14.2518 10.4501 14.28C11.4672 14.2723 12.4411 13.8676 13.1641 13.1521C13.887 12.4366 14.3019 11.467 14.3201 10.45Z" fill="#DAC2A8"/>
            </svg>
            <?php 
            if(!empty($userRow['user_city']) && $userRow['user_country']){ 
            ?>
              <span><?php echo $userRow['user_city']; ?>, <?php echo $userRow['user_country']; ?></span>
            <?php
            } else if(!empty($userRow['user_city']) && empty($userRow['user_country'])){
            ?>
              <span><?php echo $userRow['user_city']; ?></span>
            <?php 
            } else if(empty($userRow['user_city']) && !empty($userRow['user_contry'])){
            ?>
              <span><?php echo $userRow['user_country']; ?></span>
            <?php 
            } else{
            ?>
              <span>Не указано</span>
            <?php 
            }
            ?>
          </div>
        <div class="profile__info-about"><span>О пользователе</span>
          <p><?php echo $userRow['user_info']; ?></p>
        </div>
        <div class="profile__info-contacts"><span>Контакты:</span>
          <button>Показать контакты</button>
        </div>
      </div>
    </div>
  </section>
      <section style="margin:0 auto;">
      <?php
          if( !empty($loggedUser) && $userRow['user_email'] == $loggedUser ){
      ?>
          <h2 class="section-title">Ваши коты:</h2>
      <?php
          } else{
      ?>
          <h2 class="section-title">Коты пользователя:</h2>
      <?php
          }
        }
      ?>
      <div class="profile__cats">
        <button class="pagination-button profile-pagination" id="prev-button">
          <svg width="49" height="98" viewBox="0 0 49 98" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M44.5 4L8 52.5L44.5 94" stroke="#DAC2A8" stroke-width="11"/>
          </svg>
        </button>
      <ul class="profile__list" id="paginated-list" data-current-page="1">
        <?php
            $cat_query = "SELECT * FROM cats WHERE users_id LIKE '$userId';";
            $cat_result = mysqli_query($connection, $cat_query);
            while($catRow = mysqli_fetch_assoc($cat_result)){
        ?>
          <li class="profile__list-item" style="overflow:hidden;margin:0 auto;"><a href="cat.php?cat=<?php echo $catRow['id']; ?>"><img style="width:100%;height:100%;object-fit:cover;" src="assets/img/cats/<?php echo $catRow['picture']; ?>" alt="Кот" title="<?php echo $catRow['name']; ?>"></a></li>
        <?php
          }
          ?>
      </ul>
      <button class="pagination-button profile-pagination" id="next-button">
        <svg width="49" height="98" viewBox="0 0 49 98" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path d="M5 94L41.5 45.5L5.00001 4" stroke="#DAC2A8" stroke-width="11"/>
        </svg>
      </button>
      </div>
    </div>
  </section>
  <script src="assets/js/profile.js"></script>
</main>
<?php
} else if(empty($_COOKIE['user']) && empty($_GET['user'])){
  ?>
<main class="main">
  <section class="register"> 
    <div class="container">
      <div class="register-inner">
        <form class="register-block" action='header.php' method="POST">
          <h2 class="section-title">Регистрация</h2>
          <span style="display:block;text-align:center;">Для входа введите почту и пароль</span>
          <div class="register-inputs">
            <div class="register-input">
              <label for="name">Ваше имя:</label>
              <input type="text" name='username' id="name">
            </div>
            <div class="register-input">
              <label for="email">Email:</label>
              <input type="email" name='email' id="email" required>
            </div>
            <div class="register-input">
              <label for="tel">Телефон:</label>
              <input type="tel" name='tel' id="tel">
            </div>
            <div class="register-input">
              <label for="pass">Пароль:</label>
              <input type="password" name='password' id="pass" required>
            </div>
          </div>
          <button type="submit" name="submit">Вступить в клуб</button>
        </form>
      </div>
    </div>
  </section>
  <script src="assets/js/profile.js"></script>
</main>
<?php
}
?>
<?php
require 'footer.php';
?>