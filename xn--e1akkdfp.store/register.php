<?php
require 'header.php';
if(empty($_COOKIE['user'])){
?>
<main class="main">
  <section class="register"> 
    <div class="container">
      <div class="register-inner">
        <form class="register-block" action='header.php' method="POST">
          <h2 class="section-title">Регистрация</h2>
          <span style="display:block;text-align:center;">Для входа введите почту и пароль</span>
          <div class="register-inputs">
            <div class="register-input">
              <label for="name">Ваше имя:</label>
              <input type="text" name='username' id="name">
            </div>
            <div class="register-input">
              <label for="email">Email:</label>
              <input type="email" name='email' id="email" required>
            </div>
            <div class="register-input">
              <label for="tel">Телефон:</label>
              <input type="tel" name='tel' id="tel">
            </div>
            <div class="register-input">
              <label for="pass">Пароль:</label>
              <input type="password" name='password' id="pass" required>
            </div>
          </div>
          <button type="submit" name="submit">Вступить в клуб</button>
        </form>
      </div>
    </div>
  </section>
</main>
<?php
} else{
	header("Location: profile.php");
}
require 'footer.php';
?>